import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Profiili from "./components/Profiili";
import Kooste from "./components/Kooste";
import ProjectText from "./components/ProjectText";

function App() {
  return (
    <div className="App">
      <div className="InnerDiv">
        <Profiili />

        <ProjectText
          title="Javascript | Git - Harjoitus 10.9.2020"
          content="Kyseessä oli perus javascriptin toiminnan oppiminen. 
          Sekä gitin kertaaminen. Todella pieni laajus joten ei ole paljon sanottavaa.[b]
          
          En oppinut paljoakaan kaikki oli jo entuudestaan tuttua. En tiedä kuinka
          tarpeellinen gittiin puskeminen oli tässä tilanteessa."
          stack="VanillaJS"
          link="https://gitlab.com/juha_airaksinen/urheilu1"
          site="gitlab"
        />

        <ProjectText
          title="Express.js | Node.js | MariaDB - Harjoitus 16.9.2020"
          content="Harjoituksesssa tein express palvelimen joka käytti REST apia.
            Tarkoitus oli hakea tietoja MariaDB tietokannasta. Stored Procedurejen
            avulla. Express pyöri node.js:ssä. [b]

            Tiedot asetettiin HTML taulukkoon javascriptin avulla. Asettelu
            suoritettiin erisellä CSS tiedostolla. Opin ehkä eniten CSS asettelua. [b]

            Opin REST apia, Express.js käyttöä. Node.js oli entuudestaan hiukan
            tuttu. Tuli kertausta HMTL ja CSS käytöstä sekä SQL:sta."
          stack="HTML css npm Express.js Node.js REST MariaDB"
          link="https://gitlab.com/juha_airaksinen/express-webdev"
          site="gitlab"
        />

        <ProjectText
          title="React | Bootstrap - Harjoitus 19.10.2020"
          content="Harjoituksessa luotiin taas urheilija taulukko, mutta Reactissa ja 
            käytettiin apuna bootstrap visuaalisia asettelu tyylejä. React
            componenttien käyttö. Tarkoitus oli tehdä käyttöliittymä tietokantaan.
            Jolla pystyi lisäämään, poistamaan ja muokkamaan rivejä tietoja.
            Poistaminen ja lisääminen toimivat hyvin. [b]

            Tehtävä jäi kesken ennen palautusta, tarkoitus oli tehdä modal ikkuna
            jonka kautta muokattaisiin rivin tietoja. [b]

            Opin käyttämään reactia, bootstrap asetteluja. Axios oli käytössä
            myös. En ehkä hyödyntänyt React componentteja riittävästi."
          stack="React.js Bootstrap HTML css npm Express.js Node.js REST MariaDB"
          link="https://gitlab.com/juha_airaksinen/urheilu2"
          site="gitlab"
        />

        <ProjectText
          title="Laivanupotus - Harjoitus 28.10.2020"
          content="Harjoituksessa loin laivanupotus kenttä generaattorin. 
            Luo yhden 3  pituisen laivan satunnaiseen paikkaan 5x5 gridillä. 
            Voi olla kolumnissa tai rivillä. [b]
            
            CSS asettelu riippui oliko kyseessä tyhjä solu taulukossa vai oliko kyseessä 'laiva'"
          stack="HTML css VanillaJS Express.js Node.js"
          link="https://gitlab.com/juha_airaksinen/laivanupotus"
          site="gitlab"
        />

        <ProjectText
          title="Portfolio - Harjoitus 30.10.2020"
          content="Harjoituksessa loin tämän portfolio sivun. Päätin tehdä melko
            minimalistisen ulkoasun. Harkitsin lisätä kuvia ja kuvakkeita, 
            mutta mielestäni se ei ollut tarpeellista. [b]
            
            Tarkoitus oli luoda helposti päivitettävä pohja johon helppo lisätä tekstiä yms.
            Todennäköisesti en tarkalleen ottaen tätä portfoliota käyttäisi julkisesti, mutta
            saattaisin käyttää osia tämän ratkaisun komponenteista. [b]

            Suurin ongelma ehkä tässä portfoliossa on projektit jotka ovat esillä.
            En koe, että ne osoittavat taitojani hyvin, koska ne ovat erittäin aloittelija tyylisiä.
            "
          stack="HTML css npm Express.js Node.js"
          link="https://gitlab.com/juha_airaksinen/portfolio-kurssi"
          site="gitlab"
        />

        <Kooste />
      </div>
    </div>
  );
}

export default App;
