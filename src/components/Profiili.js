import React from "react";
import Card from "react-bootstrap/Card";

function Profiili() {
  return (
    <Card bg="dark" text="light" className="Cards">
      <Card.Body>
        <Card.Title className="CardTitle">
          Juha Airaksinen | Portfolio
        </Card.Title>
        <Card.Link
          href="https://gitlab.com/juha_airaksinen"
          className="App-link"
        >
          gitlab
        </Card.Link>
        <text> | </text>
        <Card.Link href="https://github.com/JuhaAir" className="App-link">
          github
        </Card.Link>
        <text> | </text>
        <Card.Link
          href="https://www.linkedin.com/in/juha-airaksinen-7391241a6/"
          className="App-link"
        >
          linkedin
        </Card.Link>
        <text> | </text>
        <Card.Link
          href="https://hackmd.io/63AAGgBOTuCr4yo_FDR3Ww"
          className="App-link"
        >
          blogi
        </Card.Link>
      </Card.Body>
    </Card>
  );
}
export default Profiili;
