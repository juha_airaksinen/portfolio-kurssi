import React from "react";
import Card from "react-bootstrap/Card";

function ProjectText(props) {
  const tokenized = props.content.split("[b]"); //custom linebreak \n didn't work

  const items = tokenized.map((paragraph) => (
    <Card.Text className="CardContent">{paragraph}</Card.Text>
  ));

  const modifiedStack = props.stack.replaceAll(" ", " | ");
  return (
    <Card bg="dark" text="light" className="Cards">
      <Card.Body>
        <Card.Title className="CardTitle">{props.title}</Card.Title>
        {items}
        <Card.Subtitle className="StackContent">
          {"stack: " + modifiedStack}
        </Card.Subtitle>
        <Card.Link className="App-link" href={props.link}>
          {props.site}
        </Card.Link>
      </Card.Body>
    </Card>
  );
}

export default ProjectText;
