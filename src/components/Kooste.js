import React from "react";
import Card from "react-bootstrap/Card";

function Kooste() {
  return (
    <Card bg="dark" text="light" className="Cards">
      <Card.Body>
        <Card.Title className="CardTitle">
          LTD7003-LTA20V1 Webdev kurssi
        </Card.Title>
        <div className="CardContent">
          <Card.Subtitle className="CardSubTitle">Lähtötilanne:</Card.Subtitle>
          <Card.Text className="CardContent">
            Olen ollut pintakosketuksissa javascriptin ja node.js kanssa. Vapaa
            ajallani olin tehnyt chatbotin twitchiin. Kyseessä oli simppeli
            ohjelma. Opin siinä vähän js perusteita ja kuinka node pyörii
            yhdellä threadilla asynccinä. HTML ja CSS taisin olla jo kokeillut
            yläasteella.
          </Card.Text>
          <Card.Subtitle className="CardSubTitle">Käytetty aika:</Card.Subtitle>
          <Card.Text className="CardContent">
            Alussa kurssi ehkä oli helpompi, koska oli aikaisempaa kokemusta
            aiheista. Mahdollisesti jos ei olisi ollut niin kokonaisuudessa
            olisi mennyt enemmän aikaa. Osassa tehtäviä meni aikaa melko
            paljonkin. Suurin osa ajasta meni työkalujen tutkimiseen ja
            suunnitteluun.
          </Card.Text>
          <Card.Subtitle className="CardSubTitle">
            Kurssin positiivisia ja huonoja:
          </Card.Subtitle>
          <Card.Text className="CardContent">
            Projektien laajus oli ehkä liian pieni. Kokonaisuudessa tuntui että
            juuri kun pääsi vauhtiin jossain aiheessa tehtävä kerkesi jo loppua.
            Mahdollisesti myös liikaa aloitus tason tehtäviä?
          </Card.Text>
          <Card.Subtitle className="CardSubTitle">
            Sainko tarpeeksi apua / palautetta:
          </Card.Subtitle>
          <Card.Text className="CardContent">
            Omasta mielestäni opin parhaiten tutkimalla aiheita itsenäisesti,
            mutta tehtävistä nopeampi palaute auttaisi parantamaan itseäni
            nopeammin. Näin saisi tietää mikä oikeasti meni hyvin. Mikä meni
            huonosti eli missä olisi parannettavaa. Jos palaute jää myöhäiseksi
            saattaa unohtaa mitä teki kyseisen tehtävän kanssa.
          </Card.Text>
          <Card.Subtitle className="CardSubTitle">Kohokohdat</Card.Subtitle>
          <Card.Text className="CardContent">
            Mahdollisesti Express.js ja REST api. Nämä ehkä yleishyödyllisimpiä
            aiheita. React oli myös ihan kiinnostava aihe, opin ehkä eniten
            tehdessäni tätä portfoliota. Olen ehkä vasta raapaissut pintaa.
            Omissa piireissä olen kuullut että GraphQL ja TypeScript ovat hyviä
            työkaluja.
          </Card.Text>
          <Card.Subtitle className="CardSubTitle">Tulevaisuus</Card.Subtitle>
          <Card.Text className="CardContent">
            Pyrin kehittämään osaamistani aina ja mahdollisimman laajasti. Koen
            että webdev olisi hyvä työympäristöltään verrattuna pelien
            kehittämiseen ja liiketoiminta ei sinäänsä minua kiinnosta. Jatkan
            uusien työkalujen oppimista ja niiden käyttämistä. Vaikea sanoa
            mille alalle loppujen lopuksi päädyn.
          </Card.Text>
        </div>
      </Card.Body>
    </Card>
  );
}

export default Kooste;
